const { Router } = require('express');
const router = Router();

const { getUsers,createUser} = require('../controller/users.controller');

router.route('/')
    .get(getUsers)
    .post(createUser);

module.exports = router;