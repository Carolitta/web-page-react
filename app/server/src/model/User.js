const { Schema, model } = require('mongoose');

const userSchema = new Schema(
    {
        username: {
            type: String,
            required: true,
            unique: true,
            trim: true
        },
        email: { type: String, trim: true },
        content: { type: String, required: true},
        date: Date
    }, {
        timestamps: true
    });

module.exports = model('User', userSchema);