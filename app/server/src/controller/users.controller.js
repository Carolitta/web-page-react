const userCtrl = {};

const User = require('../model/User');

userCtrl.getUsers = async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    }
    catch (err) {
        res.status(400).json({
            error: err
        });
    }
};

userCtrl.createUser = async (req, res) => {
    try {
        const { username, email, content, date } = req.body;

        const newUser = new User({ 
            username,
            
            email,
            content,
            date
        });
        await newUser.save();
        res.json('Usuario creado');
    } catch (e) {
        console.log(e)
        res.json(e.errmsg);
    }
};

module.exports = userCtrl;