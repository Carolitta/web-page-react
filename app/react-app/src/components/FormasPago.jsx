import React, { Component, Fragment } from 'react'
import Grid from '@material-ui/core/Grid';
import './assets/css/FormasPago.css'
import Linea from './assets/img/23.jpg'
import Centro from './assets/img/26.jpg'
import Tres from './assets/img/bank.jpg'

export default class FormasPago extends Component {
    render() {
        return (
            <Fragment>
            <div className="box-gralFP">
                <Grid container>
                    <Grid item md={4}>
                        <div className="box-img1FP">
                            <div className="img--titleFP">
                                <p><span className="titleFP--span" id={"FP"}>Formas de Pago<br/></span><span className="subtitleFP--span">En Línea<br/></span>
                                    Lorem ipsum dolor sit amet, consectetuer
                                    adipiscing elit, sed diam nonummy nibh
                                </p>
                            </div>
                            <img className="img--lineaFP" src={Linea} alt="17clara" />
                        </div>
                    </Grid>
                    <Grid item md={4}>
                    <div className="box-img2FP">
                            <div className="img--titleFP">
                                <p><span className="subtitleFP--span">Centro de Extensión Académica<br/></span>
                                    Lorem ipsum dolor sit amet, consectetuer
                                    adipiscing elit, sed diam nonummy nibh
                                </p>
                            </div>
                            <img className="img--lineaFP" src={Centro} alt="17clara" />
                        </div>
                    </Grid>
                    <Grid item md={4}>
                    <div className="box-img3FP">
                            <div className="img--titleFP">
                                <p><span className="subtitleFP--span">En Banco<br/></span>
                                    Lorem ipsum dolor sit amet, consectetuer
                                    adipiscing elit, sed diam nonummy nibh
                                </p>
                            </div>
                            <img className="img--lineaFP" src={Tres} alt="17clara" />
                        </div>
                        
                    </Grid>
                </Grid>
            </div>
            </Fragment>
        )
    }
}
