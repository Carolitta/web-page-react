import React, { Component } from 'react'
import './assets/css/Flyer.css'
import Img from './assets/img/27c.jpg';


export default class FlyerAnalisisCurso extends Component {
    render() {
        return (
            <div className="bg" id={"topAC"}>
                <div className="flex abs mov">
                    <div className="flex titulo">
                        <div className="texto_t">Análisis de Datos</div>
                    </div>
                </div>
                <img className="d-block w-100 crl" src={Img} alt="First slide"/>
            </div>
        )
    }
}
