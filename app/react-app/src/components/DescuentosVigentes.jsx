import React, { Component } from 'react'
import './assets/css/DescuentosVigentes.css'

export default class DescuentosVigentes extends Component {
    render() {
        return (
            <div className="box-descuentos">
                <div className="grid-containerLeft">
                    <div className="g-cL__item--title" id={"DV"}>
                        <h3>Descuentos vigentes</h3>
                    </div>
                    <div className="g-cL__item--content">
                        <h5>Como aplicarlos</h5>
                        <p>Lorem ipsum, dolor sit amet consectetur<br/>
                            adipisicing elit. Corporis excepturi <br/>
                            sequi inventore dignissimos veniam <br/>
                            accusantium culpa doloremque dolorem <br/>
                            voluptas rem error, perferendis a labore<br/>
                            deserunt nobis, quisquam ullam beatae<br/>
|                       </p>
                    </div>
                </div>
                <div className="grid-containerRight">
                    <div className="g-cR__item">
                        <h6>UNAM</h6>
                        <p>50%</p>
                    </div>
                    <div className="g-cR__item">
                        <h6>Desempeño en curso previo (no aplica en diplomados)</h6>
                        <p>10%</p>
                    </div>
                    <div className="g-cR__item">
                        <h6>Exalumnos de la UNAM</h6>
                        <p>10%</p>
                    </div>
                    <div className="g-cR__item">
                        <h6>Escuelas incorporadas a la UNAM e Instituciones educativas con reconocimiento
                    oficial de la SEP</h6>
                        <p>20%</p>
                    </div>
                </div>
            </div>
        )
    }
}
