import React, { Component } from 'react'
import './assets/css/DescripcionDiplomadoCurso.css'

export default class DescA2Curso extends Component {
    render() {
        return (
            <div className="contenido" id={"curso2"}>
                <div className="box-titletext">
                    <div className="title__left">
                        Cursos
                </div>
                    <div className="text__right">
                        Nuestra oferta académica es muy diversa e incluye desde cursos básicos e introductorios hasta aquellos
                        de alto nivel de especialización. Las sesiones tienen un enfoque práctico para apoyar a los
                        participantes lograr un completo dominio de las temáticas. A quienes aprueban los cursos se les entrega
                        constancia con registro
                </div>
                </div>
            </div>
        )
    }
}
