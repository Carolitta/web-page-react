import React, { Component, Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import './assets/css/Inicio.css'
import Aprende from './assets/img/4.jpg'
import Desarrollo from './assets/img/A2.jpg'
import Disenio from './assets/img/A3.jpg'
import Carousel from './Carousel'
import FooterIni from './FooterIni';
import Submenu from './Submenu';
import { Link } from 'react-router-dom'

//var URLdomain = window.location.host;
//alert(URLdomain);


class Inicio extends Component {
    render() {
        return (
            <Fragment>
                <Submenu />
                <Carousel />
                <div className="cuerpo">
                    <div className="intro">
                        <div className="flex ">
                            <div className="flex areas">
                                Áreas de conocimiento
                        </div>
                            <div className="text_areas">
                                Nuestra oferta académica es muy diversa e incluye desde cursos básicos e introductorios hasta aquellos de alto nivel de especialización. Las sesiones tienen un enfoque práctico para apoyar a los participantes lograr un completo dominio de las temáticas. A quienes aprueban los cursos se les entrega constancia con registro
                        </div>
                        </div>
                        <div className="numeros">
                            <Grid container>
                                <Grid item md={4}>
                                    <div className="relativo bases">
                                        <div className="abs numero_titulo">
                                            Bases de datos
                                    </div>
                                        <div className="numero">01</div>
                                        <div className="text_titulo">
                                            Lorem ipsum dolor sit amet consectetur
                                    </div>
                                        <div className="relativo imagen">
                                       <Link to={{
                                                    pathname:'/a1',
                                                    hash:'#topA1'
                                                        }}><img className="relativo" src={Aprende} alt="d" /></Link>
                                            {/* <a href="http://sofia.intranet.ingenieria.unam.mx/a1"><img className="relativo" src={Aprende} alt="d" /></a>*/}
                                        </div>
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="relativo desarrollo">
                                        <div className="abs numero_titulo">
                                            Desarrollo de aplicaciones <br />para moviles
                                    </div>
                                        <div className="numero">02</div>
                                        <div className="text_titulo">
                                            Lorem ipsum dolor sit amet consectetur
                                    </div>
                                        <div className="relativo imagen_des">
                                        <Link to="/a2"><img className="relativo" src={Desarrollo} alt="d" /></Link>
                                            {/*<a href="http://sofia.intranet.ingenieria.unam.mx/a2"><img className="relativo" src={Desarrollo} alt="d" /></a>*/}
                                        </div>
                                    </div>

                                </Grid>
                                <Grid item md={4}>
                                    <div className="relativo disenio">
                                        <div className="abs numero_titulo">
                                            Diseño editorial y grafico
                                    </div>
                                        <div className="numero">03</div>
                                        <div className="text_titulo">
                                            Lorem ipsum dolor sit amet consectetur
                                    </div>
                                        <div className="relativo imagen_dis">
                                        <Link to="/a3"><img className="relativo" src={Disenio} alt="d" /></Link>
                                            {/*<a href="http://sofia.intranet.ingenieria.unam.mx/a3"><img className="relativo" src={Disenio} alt="d" /></a>*/}
                                        </div>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </div>
                <FooterIni />
            </Fragment>
        );
    }
}

export default Inicio;