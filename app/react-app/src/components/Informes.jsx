import React, { Component, Fragment } from 'react';
import FlyerInfo from './FlyerInfo';
import DescuentosVigentes from './DescuentosVigentes';
import FooterInfo from './FooterInfo';
import FormasPago from './FormasPago';
import Infosubmenu from './Infosubmenu'
import InformesPI from './InformesPI'
class Informes extends Component {
    render() {
        return (
            <Fragment>
                <Infosubmenu/>
                <FlyerInfo/>
                <InformesPI/>
                <FormasPago/>
                <DescuentosVigentes/>
                <FooterInfo/>
            </Fragment>
        );
    }
}

export default Informes;