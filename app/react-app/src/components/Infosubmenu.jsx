import React, { Component,Fragment } from 'react'
import './assets/css/Submenu.css'
import { Link } from 'react-scroll'

export default class Infosubmenu extends Component {
    render() {
        return (
            <Fragment>
                <div className="wrap__submenu--Info">
                    <div className="submenu__menu">
                        <nav>
                            <label className="left__area">Informes</label>
                            <ul className="right__submenu--ul">
                            <Link to="PI" smooth={true} duration={1000}><li>Proceso de inscripción</li></Link>
                                <Link to="FP" smooth={true} duration={1000}><li className="right__C">Formas de pago</li></Link>
                                <Link to="DV" smooth={true} duration={1000}><li>Descuentos Vigentes</li></Link>
                            </ul>
                        </nav>
                    </div>
                    <div className="submenu__line--Info"></div>
                </div>
            </Fragment>
        )
    }
}
