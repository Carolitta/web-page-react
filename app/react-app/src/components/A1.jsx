import React, { Component, Fragment } from 'react';
import Flyer from './Flyer';
import FooterA1 from './FooterA1';
import LineasTextA1 from './LineasTextA1';
import DescA1Diplom from './DescA1Diplom'
import DiplomadosA1 from './DiplomadosA1';
import DescA1Curso from './DescA1Curso'
import CursosA1 from './CursosA1';
import A1submenu from './A1submenu';
//var URLdomain = window.location.host;
//alert(URLdomain);
class A1 extends Component {
    render() {
        return (
            <Fragment>
                <A1submenu/>
                <Flyer/>
                <LineasTextA1/>
                <DescA1Diplom/>
                <DiplomadosA1/>
                <DescA1Curso/>
                <CursosA1/>
                <FooterA1/>
            </Fragment>
        );
    }
}

export default A1;