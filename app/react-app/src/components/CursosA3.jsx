import React, { Component } from 'react'
import LeftDark from './assets/img/20c.jpg'
import RightLight from './assets/img/21c.jpg'
import './assets/css/ContenidoAreas.css'

export default class CursosA3 extends Component {
    render() {
        return (
            <div className="box-imglrC">
                <div className="box-img__left--up">
                    <img className="img--dark" src={LeftDark} alt="15obscura" />
                    <div className="img--title">
                        <h4>Diseño<br/>Editorial<br/></h4>
                        <h5><br />Inicia: 27 Abril</h5>
                        <h6><br />Ver más</h6>
                    </div>
                </div>
                <div className="box-img__right--down">
                    <img className="img--light" src={RightLight} alt="17clara" />
                    <div className="img--title">
                        <h4>Curso de<br/>Photoshop<br/>Básico<br/></h4>
                        <h5><br />Inicia: 27 Abril</h5>
                        <h6><br />Ver más</h6>
                    </div>
                </div>
            </div>
        )
    }
}
