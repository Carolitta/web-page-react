import React, { Component, Fragment } from 'react'
import './assets/css/AnalisisProfesores.css'
import Icon from '@material-ui/core/Icon';

export default class AnalisisProfesores extends Component {
    render() {
        return (
            <Fragment>
                <div className="teacher-container">
                    <div className="teacher-container__title">
                        <span>Profesores</span>
                    </div>
                    <div className="grid-container__teacher">
                        <div className="grid-item__1teacher">
                            <div className="item-1teacher__card">
                                <span>Nombre Apellidos</span>
                                <p>Lorem ipsum</p>
                            </div>
                            <div className="item-1teacher__icons">
                                <Icon className="fa fa-facebook icon__fb" />
                                <Icon className="fa fa-twitter icon__tw" />
                                <Icon className="fa fa-instagram icon__insta" />
                                <Icon className="fa fa-youtube-play icon__youtube" />
                            </div>
                        </div>
                        <div className="grid-item__2teacher">
                            <div className="item-2teacher__card">
                                <span>Nombre Apellidos</span>
                                <p>Lorem ipsum</p>
                            </div>
                            <div className="item-2teacher__icons">
                                <Icon className="fa fa-facebook icon__fb" />
                                <Icon className="fa fa-twitter icon__tw" />
                                <Icon className="fa fa-instagram icon__insta" />
                                <Icon className="fa fa-youtube-play icon__youtube" />
                            </div>
                        </div>
                        <div className="grid-item__3teacher">
                            <div className="item-3teacher__card">
                                <span>Nombre Apellidos</span>
                                <p>Lorem ipsum</p>
                            </div>
                            <div className="item-3teacher__icons">
                                <Icon className="fa fa-facebook icon__fb" />
                                <Icon className="fa fa-twitter icon__tw" />
                                <Icon className="fa fa-instagram icon__insta" />
                                <Icon className="fa fa-youtube-play icon__youtube" />
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
