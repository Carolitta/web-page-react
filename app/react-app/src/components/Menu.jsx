import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './assets/css/Menu.css'
import UNAM from './assets/img/contitulo.png';
import USECAD from './assets/img/USECAD.png'
//import { Link } from 'react-scroll'
//import Scroll from 'react-scroll'
//const ScrollLink = Scroll.ScrollLink


class Menu extends Component {
    render() {
        return (
            <div className="flex abs menu">
                <div className="logos-container">
                    <span className="logo__left">
                        <img className="logo__left--img" src={UNAM} alt="d" />

                    </span>
                    <div className="linea"></div>
                    <span className="logo__right">
                        <img className="logo__right--img" src={USECAD} alt="d" />
                    </span>
                </div>
                <div className="enlaces_menu">
                    <ul className="flex">
                        <li><Link to="/">Inicio</Link></li>
                        <li>
                            <Link to="/a1">Área 1</Link>
                        </li>
                        <li>
                            <Link to="/a2">Área 2</Link>
                        </li>
                        <li>
                            <Link to="/a3">Área 3</Link>
                        </li>
                        <li>
                            <Link to="/info">Informes</Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Menu;