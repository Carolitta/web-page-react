import React, { Component, Fragment } from 'react'
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import './assets/css/Footer.css'

export default class FooterA1 extends Component {
    render() {
        return (
            <Fragment>
                <div className="relativo footerA1">
                    <Grid container>
                        <Grid item md={4}>
                            <div className="flex fac">
                                Facultad de
                            <h2>Ingeniería</h2>
                            </div>
                        </Grid>
                        <Grid item md={4}>
                            Universidad Nacional Autónoma de MéxicoFacultad de Ingeniería, Av. Universidad 3000, Ciudad Universitaria, Coyoacán, Cd. Mx., CP 04510
                        <div className="mas">
                                Teléfono: 56 22 08 66 <br />
                            Fax: 56 16 28 90 <br />
                            eMail: fainge@unam.mx
                        </div>
                        </Grid>
                        <Grid item md={4}>
                            <div className="flex iconos">
                                <div className="icono">
                                    <Icon className="fab fa-facebook-f" />
                                </div>
                                <div className="icono">
                                    <Icon className="fab fa-twitter" />
                                </div>
                                <div className="icono">
                                    <Icon className="fab fa-instagram" />
                                </div>
                                <div className="icono">
                                    <Icon className="fab fa-youtube" />
                                </div>
                            </div>
                        </Grid>
                        <Grid item md={12}>
                            <div className="copy">
                                Todos los derechos reservados © 1999 - 2019 / Facultad de Ingeniería/UNAM/Esta es la página electrónica institucional de la Facultad de Ingeniería de la UNAM. Puede ser reproducida con fines no lucrativos, siempre y cuando no se mutile, se cite la fuente completa y su dirección electrónica. Contiene enlaces con diversos portales de entidades y organizaciones académicas, estudiantiles y profesionales, así como con páginas personales de profesores e investigadores cuyos contenidos son de la responsabilidad exclusiva de sus titulares.
                        </div>
                        </Grid>
                    </Grid>
                </div>
            </Fragment>
        )
    }
}
