import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import './assets/css/InformesPI.css'

export default class InformesPI extends Component {
    render() {
        return (
            <div className="PI-container">
                <Grid container>
                    <Grid item md={12}>
                        <div className="PI__title" id={"PI"}><span>Poceso de Inscripción</span></div>
                    </Grid>
                </Grid>
                <Grid container className="contenedor-2">
                    <Grid item md={4}>
                        <div className="PI__item01">

                            <span className="PI__number">01</span><span>Cursos<br /></span>
                            <p className="PI__item--p">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh
                            euismod tincidunt ut laoreet dolore magna
                            aliquam erat volutpat. Ut wisi enim ad minim
                            veniam, quis nostrud exerci tation ullamcorper
                            suscipit lobortis nisl ut aliquip
                                </p>
                        </div>
                    </Grid>
                    <Grid item md={4}>
                        <div className="PI__item02">

                            <span className="PI__number">02</span><span>Pre registro<br /></span>
                            <p className="PI__item--p">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh
                            euismod tincidunt ut laoreet dolore magna
                            aliquam erat volutpat. Ut wisi enim ad minim
                            veniam, quis nostrud exerci tation ullamcorper
                            suscipit lobortis nisl ut aliquip
                            </p>
                        </div>
                    </Grid>
                    <Grid item md={4}>
                        <div className="PI__item03">

                            <span className="PI__number">03</span><span>Inscripción<br /></span>
                            <p className="PI__item--p">Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh
                            euismod tincidunt ut laoreet dolore magna
                            aliquam erat volutpat. Ut wisi enim ad minim
                            veniam, quis nostrud exerci tation ullamcorper
                            suscipit lobortis nisl ut aliquip
                            </p>
                        </div>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item md={12}>
                        <div className="PI__item04">

                            <span className="PI__number">04</span><span>Notificación<br /></span>
                            <p className="PI__item--p"> Lorem ipsum dolor sit amet, consectetuer
                            adipiscing elit, sed diam nonummy nibh
                            euismod tincidunt ut laoreet dolore magna
                            aliquam erat volutpat. Ut wisi enim ad minim
                            veniam, quis nostrud exerci tation ullamcorper
                            suscipit lobortis nisl ut aliquip
                            </p>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}
