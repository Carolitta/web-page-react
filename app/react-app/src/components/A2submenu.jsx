import React, { Component, Fragment } from 'react'
import './assets/css/Submenu.css'
import { Link } from 'react-scroll'

export default class A2submenu extends Component {
    render() {
        return (
            <Fragment>
                <div className="wrap__submenu--A2">
                    <div className="submenu__menu">
                        <nav>
                            <label className="left__area">Área Desarrollo de aplicaciones para moviles</label>
                            <ul className="right__submenu--ul">
                                <Link to="dip2" smooth={true} duration={1000}><li className="right__D">Diplomados</li></Link>
                                <Link to="curso2" smooth={true} duration={1000}><li className="right__C">Cursos</li></Link>
                            </ul>
                        </nav>
                    </div>
                    <div className="submenu__line--A2"></div>
                </div>
            </Fragment>
        )
    }
}
