import React, { Component, Fragment } from 'react'
import './assets/css/Submenu.css'
import {Link} from 'react-scroll'

export default class A1submenu extends Component {
    render() {
        return (
            <Fragment>
                <div className="wrap__submenu--A1">
                    <div className="submenu__menu">
                        <nav>
                            <label className="left__area" id="hi">Área Bases de Datos </label>
                            <ul className="right__submenu--ul">
                                <Link to="dip1" smooth={true} duration={1000}><li className="right__D">Diplomados</li></Link>
                                <Link to="curso1" smooth={true} duration={1000}><li className="right__C">Cursos</li></Link>
                            </ul>
                        </nav>
                    </div>
                    <div className="submenu__line--A1"></div>
                </div>
            </Fragment>
        )
    }
}
