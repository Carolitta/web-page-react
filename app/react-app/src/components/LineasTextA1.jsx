import React, { Component } from 'react'
import './assets/css/LineasTextA1.css'

export default class LineasTextA1 extends Component {
    render() {
        return (
            <div className="contenedor">
                <div className="box--green">
                    <div className="text">
                        <p>TODO DIPLOMADO / CURSO DEMANDA SU RESPECTIVO NIVEL DE CONOCIMIENTO<br/><br/>SI DESEAS ENTRAR A UNO DE INTERMEDIO O ALTO NIVEL DEBERÁS TENER<br />CONOCIMIENTO DE EL/LOS NIVELE/S QUE LE PRECEDE.</p>
                    </div>
                </div>
                <div className="box--yellow"></div>
                <div className="box--red"></div>
            </div>
        )
    }
}
