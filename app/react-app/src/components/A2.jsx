import React, { Component, Fragment } from 'react';
import FlyerA2 from './FlyerA2';
import FooterA2 from './FooterA2';
import DescA2Diplom from './DescA2Diplom'
import DiplomadosA2 from './DiplomadosA2'
import DescA2Curso from './DescA2Curso'
import CursosA2 from './CursosA2';
import A2submenu from './A2submenu';
//var URLactual = window.location;
//alert(URLactual);
class A2 extends Component {
    render() {
        return (
            <Fragment>
                <A2submenu/>
                <FlyerA2/>
                <DescA2Diplom/>
                <DiplomadosA2/>
                <DescA2Curso/>
                <CursosA2/>
                <FooterA2/>
            </Fragment>
        );
    }
}

export default A2;