import React, { Component, Fragment } from 'react'
import './assets/css/AnalisisTabla.css'

export default class AnalisisTabla extends Component {
    render() {
        return (
            <Fragment>
                <div className="box-container__table">
                <div className="table-container">
                    <h4>Temario</h4>
                    <table className="table">
                        <thead>
                            <tr>
                                <th className="table__thead--module">Módulos</th>
                                <th>Profesores asignados</th>
                                <th>Número de Sesiones</th>
                                <th>Duración (hrs)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 1: Fundamentos en administración de bases de datos</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 2: Planeación, instalación y configuración inicial de una base de datos</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 3: Administración de las estructuras de memoria</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 4: Administración de procesos de background</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 5: Administración de las estructuras lógicas y físicas de almacenamiento</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 6: Respaldos y recuperación</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 7: Estrategias de optimización, refinamiento y desempeño</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 8: Optimización de sentencias SQL</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 9: Distribución y clusterización de bases de datos</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 10: Introducción a la inteligencia de negocios (BI) y Big Data</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 11: Mejores prácticas en la inteligencia de negocios</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                            <tr>
                                <td data-label="Módulos" className="table__tbody--1col">Módulo 12: Procesos y manejo de datos históricos</td>
                                <td data-label="Profesores asignados" className="table__tbody--2col">Mark</td>
                                <td data-label="Número de Sesiones" className="table__tbody--3col">#</td>
                                <td data-label="Duración (hrs)" className="table__tbody--4col">#</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th data-label="TOTAL (hrs)" className="table__tfoot--total">TOTAL (hrs)</th>
                                <th className="table__tfoot--horas"></th>
                                <th className="table__tfoot--horas"></th>
                                <th className="table__tfoot--horas">#</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                </div>
            </Fragment>
        )
    }
}
