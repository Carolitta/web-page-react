import React, { Component } from 'react'
import './assets/css/AnalisisForm.css'
import Icon from '@material-ui/core/Icon';
import axios from 'axios'

export default class AnalisisForm extends Component {

    state = {
        username: '',
        email: '',
        content: '',
        users: []
    }

    async componentDidMount() {
        this.getUsers();
        console.log(this.state.users)
    }

    getUsers = async () => {
        const res = await axios.get('http://sofia.intranet.ingenieria.unam.mx:4000/api/users');
        //const res = await axios.get('http://localhost:4000/api/users');
        this.setState({ users: res.data });

    }


    onSubmit = async (e) => {
        e.preventDefault();//no reinicie la pagina y que envie al servidro
        const newUser = {
            username: this.state.username,
            email: this.state.email,
            content: this.state.content
        };
        await axios.post('http://sofia.intranet.ingenieria.unam.mx:4000/api/users', newUser);
        //await axios.post('http://localhost:4000/api/users', newUser);
        this.getUsers();
        this.setState({ username: '', email: '', content: '' });
        alert("Tus datos han sido enviados");
    }

    onInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    render() {
        return (
            <div className="contactform-container">
                <div className="contact__left">
                    <p><span>Contáctanos</span><br />Puedes comunicarte a través de los numeros o telefonico
                        o bien mandar tu mensaje.
                    </p>
                    <div className="iconstext__container">
                        <ul className="fa-ul icons__pos">
                            <li><Icon className="fa-li fa fa-user fa-lg icon__person" />Ing. Carlos R. Tlahuel Pérez</li>
                            <li><Icon className="fa-li fa fa-phone fa-lg icon__phone" />545778908978</li>
                            <li><Icon className="fa-li  fa fa-envelope fa-lg icon__mail"/>ctlahuel@unam.mx</li>
                        </ul>
                    </div>
                </div>
                <div className="form-right">
                    <form onSubmit={this.onSubmit} className="form">
                        <div className="form__field">
                            <label htmlFor="name">Nombre</label>
                            <input
                                name="username"
                                type="text"
                                id="name"
                                className="form__input"
                                onChange={this.onInputChange}
                                value={this.state.username}
                                required
                            />
                        </div>
                        <div className="form__field">
                            <label htmlFor="email">Correo Electrónico</label>
                            <input
                                name="email"
                                type="email"
                                id="email"
                                className="form__input"
                                onChange={this.onInputChange}
                                value={this.state.email}
                                required
                            />
                        </div>
                        <div className="form__field">
                            <label htmlFor="message">Mensaje</label>
                            <input
                                name="content"
                                id="message"
                                className="form__textarea"
                                onChange={this.onInputChange}
                                value={this.state.content}
                            />
                        </div>
                        <button className="form__submit">
                            Enviar
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}
