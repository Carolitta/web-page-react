import React, { Component } from 'react'
import './assets/css/IntroAnalisisCurso.css'

export default class IntroAnalisisCurso extends Component {
    render() {
        return (
            <div className="introduction-container">
                <div className="introduction__title--border">
                    <h4>Introducción</h4>
                </div>
                <div className="introduction__description--text">
                    <p>
                        El diplomado está compuesto de dos partes fundamentales, la administración y clusterización de
                        bases de datos y el análisis de los datos. Dentro de la primera parte el participante aprenderá a mejorar y llevar a una base de datos a un nivel superior en especial para implementar requisitos como
                        alta disponibilidad, distribución, balanceo de cargas, optimización de recursos y tolerancia afallas. En
                        la segunda parte se le proporcionarán los cimientos técnicos necesarios para que el participante
                        pueda identificar, analizar, diseñar y construir sistemas data warehouse que representan el núcleo
                        de sistemas de inteligencia de negocios y Big Data. Se le proporcionarán estrategias para trabajar
                    </p>
                </div>
            </div>
        )
    }
}
