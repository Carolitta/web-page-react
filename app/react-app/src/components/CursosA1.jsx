import React, { Component } from 'react'
import LeftDark from './assets/img/15c.jpg'
import RightLight from './assets/img/17c.jpg'
import './assets/css/LineasContenidoA1.css'
import { Link } from 'react-router-dom'
//import { Link } from '@material-ui/core'

export default class CursosA1 extends Component {
    render() {
        return (
           
            <div className="box-lineasC">
                <div className="box-img__left--upLineasC">
                <img className="img--darkLineas" src={LeftDark} alt="15obscura"/>
                    <div className="img--titleLineas">
                        <p><span>Clusterización Administración y Análisis de Bases de datos</span>
                        <br />Inicia: 27 Abril</p>
                        <button className="button button--img ">Ver más</button>
                    </div>
                </div>
                <div className="box-img__right--downLineasC">
                <img className="img--lightLineas" src={RightLight} alt="17clara"/>
                {/*<a href="http://localhost:3000/analisis#topAC"><img className="img--lightLineas" src={RightLight} alt="17clara"/></a>*/}
                {/*<Link href="http://localhost:3000/analisis#topAC"><img className="img--lightLineas" src={RightLight} alt="17clara"/></Link>*/}
                    <div className="img--titleLineas">
                        <p><span>Diplomado en Análisis de Datos</span>
                        <br />Inicia: 27 Abril</p>
                        {/*<a href="http://localhost:3000/analisis#topAC"><button className="button button--imgR ">Ver más</button></a>*/}
                        <Link to="/analisis"><button className="button button--imgR ">Ver más</button></Link>
                    </div>
                </div>
            </div>
           
        )
    }
}
