import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import './assets/css/Carousel.css'
import Img from './assets/img/7.jpg';
import Img1 from './assets/img/2.jpg';
import Img2 from './assets/img/1.jpg';


class Carousel extends Component {
    render() {
        return (
            <div className="relativo">
                <Grid container>
                    <Grid item md={12}>
                        <div id="carouselExampleIndicators" className="carousel slide img" data-ride="carousel">
                            <ol className="carousel-indicators abs flecha">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div className="carousel-inner">
                                <div className="carousel-item active relativo">
                                    <div className="flex abs mov opacity">
                                        <div className="flex titulo">
                                            <div className="texto_t">DIPLOMADO DE<br />ANÁLISIS DE <br />DATOS</div>
                                        </div>
                                        <div className="flex fecha">
                                            <div className="texto_f">Inicia 24 de Abril</div>
                                        </div>
                                    </div>
                                    <img className="d-block w-100 crl" src={Img} alt="First slide" />

                                </div>

                                <div className="carousel-item relativo">
                                    <div className="flex abs mov opacity">
                                        <div className="flex titulo">
                                            <div className="texto_t">DIPLOMADO DE<br />ADMINISTRACIÓN <br />DE REDES</div>
                                        </div>
                                        <div className="flex fecha">
                                            <div className="texto_f">Inicia 24 de Abril</div>
                                        </div>
                                    </div>
                                    <img className="d-block w-100 crl" src={Img1} alt="First slide" />

                                </div>

                                <div className="carousel-item relativo">
                                    <div className="flex abs mov opacity">
                                        <div className="flex titulo">
                                            <div className="texto_t">DIPLOMADO DE<br />CLUSTERIZACIÓN <br />DE DATOS</div>
                                        </div>
                                        <div className="flex fecha">
                                            <div className="texto_f">Inicia 24 de Abril</div>
                                        </div>
                                    </div>
                                    <img className="d-block w-100 crl" src={Img2} alt="First slide" />

                                </div>

                            </div>
                            <a className="carousel-control-prev abs flecha" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span className="sr-only">Previous</span>
                            </a>
                            <a className="carousel-control-next abs flecha" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                <span className="sr-only">Next</span>
                            </a>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default Carousel;