import React, { Component, Fragment } from 'react'
import './assets/css/Submenu.css'
import { Link } from 'react-scroll'

export default class A3submenu extends Component {
    render() {
        return (
            <Fragment>
                <div className="wrap__submenu--A3">
                    <div className="submenu__menu">
                        <nav>
                            <label className="left__area">Área Diseño editorial y grafico</label>
                            <ul className="right__submenu--ul">
                                <Link to="dip3" smooth={true} duration={1000}><li className="right__D">Diplomados</li></Link>
                                <Link to="curso3" smooth={true} duration={1000}><li className="right__C">Cursos</li></Link>
                            </ul>
                        </nav>
                    </div>
                    <div className="submenu__line--A3"></div>
                </div>
            </Fragment>
        )
    }
}
