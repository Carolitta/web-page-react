import React, { Component } from 'react'
import LeftDark from './assets/img/15c.jpg'
import RightLight from './assets/img/17c.jpg'
import './assets/css/LineasContenidoA1.css'
import { Link } from 'react-router-dom'


export default class DiplomadosA1 extends Component {
    render() {
        return (
            <div className="box-lineasD">
                <div className="box-img__left--downLineasD">
                    <img className="img--darkLineas" src={LeftDark} alt="15obscura" />
                    <div className="img--titleLineas">
                        <p><span>Clusterización Administración y Análisis de Bases de datos</span>
                            <br />Inicia: 27 Abril</p>
                        <button className="button button--img ">Ver más</button>
                    </div>
                </div>
                <div className="box-img__right--upLineasD">
                    <img className="img--lightLineas" src={RightLight} alt="17clara" />
                    {/*<a href="http://localhost:3000/analisis#topAC"><img className="img--lightLineas" src={RightLight} alt="17clara" /></a>*/}
                    <div className="img--titleLineas">
                        <p><span>Diplomado en Análisis de Datos</span>
                            <br />Inicia: 27 Abril</p>
                            <Link to="/analisis"><button className="button button--imgR ">Ver más</button></Link>
                    </div>
                </div>
            </div>
        )
    }
}
