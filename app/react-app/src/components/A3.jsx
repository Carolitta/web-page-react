import React, { Component, Fragment } from 'react';
import FlyerA3 from './FlyerA3';
import FooterA3 from './FooterA3';
import DescripcionDiplomado from './DescripcionDiplomado'
import DescripcionCurso from './DescripcionCurso'
import DiplomadosA3 from './DiplomadosA3';
import CursosA3 from './CursosA3';
import A3submenu from './A3submenu'

class A3 extends Component {
    render() {
        return (
            <Fragment>
                <A3submenu/>
                <FlyerA3/>
                <DescripcionDiplomado/>
                <DiplomadosA3/>
                <DescripcionCurso/>
                <CursosA3/>
                <FooterA3/>
            </Fragment>
        );
    }
}

export default A3;