import React, { Component } from 'react';
import './assets/css/Flyer.css'
import Img from './assets/img/ver.jpg';

class FlyerA2 extends Component {
    render() {
        return (
            <div className="bg" id={"topA2"}>
                <div className="flex abs mov">
                    <div className="flex titulo">
                        {/*<div className="texto_t">CURSOS Y<br />DIPLOMADOS <br /> <p>Desarrollo de aplicaciones móviles</p></div>*/}
                    </div>
                </div>
                <img className="d-block w-100 crl" src={Img} alt="First slide"/>
            </div>
        );
    }
}

export default FlyerA2;