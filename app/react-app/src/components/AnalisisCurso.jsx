import React, { Component, Fragment } from 'react'
import FlyerAnalisisCurso from './FlyerAnalisisCurso'
import IntroAnalisisCurso from './IntroAnalisisCurso'
import AnalisisMapa from './AnalisisMapa'
import AnalisisTabla from './AnalisisTabla'
import AnalisisProfesores from './AnalisisProfesores'
import AnalisisForm from './AnalisisForm'
import FooterAnalisis from './FooterAnalisis'

export default class AnalisisCurso extends Component {
    render() {
        return (
            <Fragment>
                <FlyerAnalisisCurso/>
                <IntroAnalisisCurso/>
                <AnalisisMapa/>
                <AnalisisTabla/>
                <AnalisisProfesores/>
                <AnalisisForm/>
                <FooterAnalisis/>
            </Fragment>
        )
    }
}
