import React, { Component } from 'react';
import './assets/css/Flyer.css'
import Img2 from './assets/img/13.jpg';

class FlyerA3 extends Component {
    render() {
        return (
            <div className="bg" id={"topA3"}>
                <div className="flex abs mov">
                    <div className="flex titulo">
                        {/*<div className="texto_t">CURSOS Y<br />DIPLOMADOS <br /> <p>Diseño editorial y gráfico</p></div>*/}
                    </div>
                </div>
                <img className="d-block w-100 crl" src={Img2} alt="First slide"/>
            </div>
        );
    }
}

export default FlyerA3;