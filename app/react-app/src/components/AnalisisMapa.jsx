import React, { Component } from 'react'
import './assets/css/AnalisisMapa.css'
import Iframe from 'react-iframe'

export default class AnalisisMapa extends Component {
    render() {
        return (
            <div className="campus-container">
                <div className="grid-container__campus">
                    <div className="grid-item__map">
                        <Iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3764.8648171341138!2d-99.18692048606557!3d19.331671448911845!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85ce00015be0a713%3A0x3fc11681a8244370!2sFacultad%20de%20Ingenier%C3%ADa%20UNAM!5e0!3m2!1ses-419!2smx!4v1592376732549!5m2!1ses-419!2smx" width="320" height="320" frameborder="0" Style="border:none; outline:none;" allowfullscreen="" aria-hidden="false" tabindex="0"></Iframe>
                        <p>Circuito Escolar 04360, C.U., CDMX<br />Coyoacán<br />04510</p>
                    </div>
                    <div className="grid-item__2col">
                        <div className="grid__2col--item">
                            <p><span>Inicio</span><br />/mayo/2020</p>
                        </div>
                        <div className="grid__2col--item">
                            <p><span>Fin</span><br />23/enero/2021</p>
                        </div>
                        <div className="grid__2col--item">
                            <p><span>Duración</span><br />horas</p>
                        </div>
                        <div className="grid__2col--item">
                            <p><span>Sede</span><br />Universitaria</p>
                        </div>
                    </div>
                    <div className="grid-item__3col">
                        <div className="grid__3col--item">
                            <p><span>Solicitudes de admisión</span><br />de Enero de 2020<br />al 06 de Mayo de 2020</p>
                        </div>
                        <div className="grid__3col--item">
                            <p><span>Periodo de inscripciones</span><br />18 de Mayo de 2020<br />al 27 de Mayo de 2020</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
