import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'


import Menu from './components/Menu'
import Inicio from './components/Inicio'
import A1 from './components/A1'
import A2 from './components/A2'
import A3 from './components/A3'
import Informes from './components/Informes';
import AnalisisCurso from './components/AnalisisCurso.jsx'
import FlyerAnalisisCurso from './components/FlyerAnalisisCurso';

function App() {
  return (
    <Router>
      <Menu/>
      <Switch>
        <Route path="/" exact component={Inicio}/>
        <Route path="/a1" exact component={A1}/>
        <Route path="/a2" exact component={A2}/>
        <Route path="/a3" exact component={A3}/>
        <Route path="/info" exact component={Informes}/>
        {/*<Route path="/analisis" exact component={AnalisisCurso}/>*/}
        <Route path="/analisis" exact render= {()=><AnalisisCurso><FlyerAnalisisCurso/></AnalisisCurso>}/>
      </Switch>
    </Router>
  );
}

export default App;
