# Landing page Educación Continua

El proyecto en desarrollo, busca dar información de diplomados y cursos dependiendo el área de conocimiento que deseen profundizar.


# Desarrollo del proyecto
Para el desarrollo del proyecto se han empleado nuevas tecnologías tanto para frontend y backend, stack MERN.

Mongo: 
- Base de datos no relacional
- Básicamente responde a tres necesidades
    - Flexibilidad
    - Escalabilidad y rendimiento
    - Siempre disponibles

Para mayor información consulte: [https://www.mongodb.com](https://www.mongodb.com "Mongo")

Express:
- Permite crear una base sólida y manejo de backend con Node

Para mayor información consulte: [https://expressjs.com](https://expressjs.com "Express")

React:
- Es una biblioteca de JavaScript para construir interfaces de usuario
- Pensada para generar una **arquitectura reusable**, **testeable** y **escalable**
- Se basa en componentes
- Declarativa por lo que la hace una aplicación rápida
- Curva de aprendizaje corta, desarrollada por Facebook y comunidad muy activa

Para mayor información consulte: [https://es.reactjs.org](https://es.reactjs.org "React")

Node:
- Ambiente de ejecución de JavaScript
- Utiliza un modelo entrada y salida sin bloqueo controlado por eventos, lo que lo hace un entorno ligero y eficiente

Para mayor información consulte: [https://nodejs.org](https://nodejs.org "Node")



# Dependencias del proyecto
### **Dependencias para el servidor**
- Node
    - npm (manejador de paquetes de node). Para mayor información consulte: [https://www.npmjs.com](https://www.npmjs.com "npm")
- MongoDB
- Servidor web
### **Dependencias para el usuario**
- Navegador web (para navegadores antiguos se necesitan *polyfills* como IE 9 e IE10)


# Estructura de carpetas

- app (aplicación)
    - react-app (frontend)
        - public (contiene los archivos estáticos)
            - USECAD.ico (icono que aparece en la pestaña de navegación)
            - index.html (contiene al id="root" aquí se renderiza toda la aplicación)
            - manifest.json (gestiona la aplicación)
        - src (carpeta donde se trabaja)
            - components (modularizando la aplicación)
                - assets (contenido utilizado en los componentes)
                    - img (imágenes utilizadas en la aplicación)
                    - css (estilos para cada componente)
            - App.css (estilos de App.js)
            - App.js (primer componente que se muestra)
            - index.css (estilos de index.js)
            - index.js (arranca la aplicación, renderiza App.js)
        - package.json (archivo de configuración del proyecto, aquí se encuentran las **dependencias del proyecto**, scripts, configuración de errores, etc)
        
    - server (backend)
        - src (carpeta donde se trabaja)
            - controller (crea las funciones que se ejecutan en el servidor)
            - model (validación de datos basados en un esquema)
            - routes (define las URL del servidor)
        - .env (definiendo las path)
        - package.json (archivo de configuración del proyecto, aquí se encuentran las **dependencias del proyecto**, scripts, etc)


# Corriendo backend (localmente)
Estos pasos se pueden realizar mientras se realizan modificaciones al proyecto
1. Posicionarse en la carpeta server 

        cd server/src
        
    1.1 Deberá instalar las dependecias que  se encuentran definidas en package.json

        npm install cors dotenv expres mongoose

    1.2 Correr el proyecto (se encuentra definido en package.json)

        npm run index

Si esta corriendo de manera exitosa mostrará lo siguiente

![doc3](doc/img/doc3.PNG)

# Corriendo frontend (localmente)
Estos pasos se pueden realizar mientras se realizan modificaciones al proyecto
1. Posicionarse en la carpeta react-app 

        cd server/react-app
        
    1.1 Deberá instalar las dependecias que  se encuentran definidas en package.json

        npm install (escribir dependencias separadas por un espacio)

    1.2 Correr el proyecto

        npm start

**Nota**: Una vez realizados los cambios sin errores puede hacer uso del comando

        npm build

# A producción
**Nota**: Mongo deberá estar corriendo en su servidor

1. En el archivo package.json de la carpeta react-app deberá definir "homepage"

2. Utilice el comando 

        npm build

La carpeta que se genere colóquela al directorio /var/www/html

# Diseño preliminar

Se trabajo con el siguiente diseño 

![doc00](doc/img/doc00.png)
![doc01](doc/img/doc01.png)
![doc03](doc/img/doc03.png)
![doc04](doc/img/doc04.png)
![doc05](doc/img/doc05.png)
![doc06](doc/img/doc06.png)

# Trabajando por componentes

 Como se menciono grosso modo lo que es React, se trabajo en componentes

![doc8](doc/img/doc8.png)

# Versión 1.0
Para esta versión se implementó

- :heavy_check_mark: Implementar stack MERN
- :heavy_check_mark: Implementar diseño visualmente
- :heavy_check_mark: Conexión con la BD

**NOTA**: hubo variaciones en el diseño durante el desarrollo

# Versión 1.1

[   ] Mejorar la implementación de componentes para el correcto enrrutado y reutilización de código
[   ] Animaciones

